//
//  AppointmentModel.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 11/24/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation


struct AppointmentModel {
    var Id:Int!
    var Hospital: HospitalModel?
    var Doctor: DoctorModel?
    var Room: RoomModel?
    var Customer: CustomerModel?
    var Service: ServiceModel?
    var Detail: DetailModel?
    var Price:Int?
    var StatusCode:Int?
    var StatusLabel:Int?
    
    mutating func parse(data: [String: Any]?) {
        
        if let hospitalData = data!["Hospital"] {
            Hospital?.initWithData(data: hospitalData as! [String : Any?])
        }
    
    }
}
