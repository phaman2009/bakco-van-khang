//
//  CustomerModel.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 10/23/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation
import UIKit

struct CustomerModel {
    var Id: Int?
    var FullName: String?
    var CustomerCode:String?
    var Phone: String?
    var HealthInsurance: String?
    var Email: String?
    var Address:String?
    var BirthDate:String?
    var Gender:Bool?
    
    mutating func initWithData(data: [String:Any]?) {
        Id = data!["Id"] as? Int
        FullName = data!["FullName"] as? String
        CustomerCode = data!["FullName"] as? String
        Phone = data!["Phone"] as? String
        HealthInsurance = data!["HealthInsurance"] as? String
        Email = data!["Email"] as? String
        Address = data!["Address"] as? String
        BirthDate = data!["BirthDate"] as? String
        Gender = data!["Gender"] as? Bool
        
    }
}

