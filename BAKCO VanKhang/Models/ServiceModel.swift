//
//  ServiceModel.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 11/24/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation

struct ServiceModel {
    var Id: Int!
    var Name: String?
    var Price:Int?
    var Image: String?
    var Code:Int?
    
    mutating func initWithData(data: [String:Any]?) {
        Id = data!["Id"] as! Int
        Name = data!["Name"] as? String
        Price = data!["Price"] as? Int
        Image = data!["Image"] as? String
        Code = data!["Code"] as? Int
    }
}
