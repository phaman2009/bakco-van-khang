//
//  HealthCareModel.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 10/23/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation

struct MatchModel {
    var HealthCareSchedulerId: Int?
    var CustomerId: Int?
    
    mutating func initWithData(data: [String:Any]?) {
        HealthCareSchedulerId = data!["HealthCareSchedulerId"] as? Int
        CustomerId = data!["CustomerId"] as? Int
        
    }
    
}
