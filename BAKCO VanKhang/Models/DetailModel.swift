//
//  DetailModel.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 11/24/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation

struct DetailModel {
    var TimeId: Int!
    var Time: String?
    var From:String?
    var To: String?
    var Active:Bool?
    
    mutating func initWithData(data: [String:Any]?) {
        TimeId = data!["TimeId"] as! Int
        Time = data!["Time"] as? String
        From = data!["From"] as? String
        To = data!["To"] as? String
        Active = data!["Active"] as? Bool
    }
}
