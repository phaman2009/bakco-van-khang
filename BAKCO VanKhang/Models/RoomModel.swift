//
//  RoomModel.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 11/24/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation
struct RoomModel {
    var Id: Int!
    var Name: String?
    var HealthCareId:Int?
    var HealthCare: Int?
    
    mutating func initWithData(data: [String:Any]?) {
        Id = data!["Id"] as! Int
        Name = data!["Name"] as? String
        HealthCareId = data!["HealthCareId"] as? Int
        HealthCare = data!["HealthCare"] as? Int
    }
}
