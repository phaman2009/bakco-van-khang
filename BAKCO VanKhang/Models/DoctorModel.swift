//
//  DoctorModel.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 11/24/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation

struct DoctorModel {
    var Id: Int!
    var FullName: String?
    var IdH:Int?
    var Image: String?
    
    mutating func initWithData(data: [String:Any]?) {
        Id = data!["Id"] as! Int
        FullName = data!["FullName"] as? String
        IdH = data!["IdH"] as? Int
        Image = data!["Image"] as? String
    }
}
