//
//  BaseViewController.swift
//  BAKCO VanKhang
//
//  Created by Kiet on 11/22/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import UIKit
import DynamicColor
import Hero

class BaseViewController: UIViewController, UIGestureRecognizerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        tabBarController?.tabBar.isTranslucent = false
        view.backgroundColor = DynamicColor(hexString: "f7f9f9")
        setupUserRightBarButton()
        isHeroEnabled = true
    }
    
    func showBackButton() {
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "green-back").withRenderingMode(.alwaysTemplate) , style: .plain, target: self, action: #selector(popToBack))
        navigationItem.leftBarButtonItem = backButton
    }
    @objc func popToBack() {
        navigationController?.popViewController(animated: true)
    }
    
    func setupUserRightBarButton() {
        let menuButon = UIBarButtonItem(image: #imageLiteral(resourceName: "green-menu-2").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(showUserMenu))
        navigationItem.rightBarButtonItem = menuButon
    }
    @objc func showUserMenu() {
        let menuVc = storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        let baseNav = BaseNavigationController(rootViewController: menuVc)
        baseNav.isHeroEnabled = true
        present(baseNav, animated: true, completion: nil)
    }
    
    func setupPopDownButton() {
        let backButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Cancel2").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(dismisss))
        navigationItem.rightBarButtonItem = backButton
    }
    @objc func dismisss() {
        hero_dismissViewController()
    }
    
    func showCancelButton() {
        let backButton = UIBarButtonItem(title: "Huỷ", style: .plain, target: self, action: #selector(dismisss))
        navigationItem.leftBarButtonItem = backButton
    }
    

}

















