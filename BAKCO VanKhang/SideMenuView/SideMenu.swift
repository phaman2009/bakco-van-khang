//
//  SideMenu.swift
//  BAKCO VanKhang
//
//  Created by Kiet on 11/23/17.
//  Copyright Â© 2017 Pham An. All rights reserved.
//

import Foundation
import UIKit

class SideMenuViewController: BaseViewController {
    
    let backDownButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "Cancel2"), for: .normal)
        btn.addTarget(self, action: #selector(dismissWithAnmation), for: .touchUpInside)
        btn.backgroundColor = .white
        return btn
    }()
    
    @objc func dismissWithAnmation() {
        hero_dismissViewController()
    }
    
    @IBOutlet var sideTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Menu"
        sideTableView.delegate = self
        sideTableView.dataSource = self
        sideTableView.register(NormalCell.self, forCellReuseIdentifier: "Cell")
        sideTableView.register(BigCell.self, forCellReuseIdentifier: "BigCell")
        sideTableView.tableFooterView = UIView()
        
        navigationController?.isHeroEnabled = true
        self.navigationController?.isNavigationBarHidden = true
        sideTableView.heroID = "conChimNon"
        sideTableView.heroModifiers = [.useScaleBasedSizeChange]
        setupBackDownButton()
    }
    
    func setupBackDownButton() {
        sideTableView.addSubview(backDownButton)
        let x = CGFloat(view.frame.width - 45.0)
        let y = CGFloat(10.0)
        let width = CGFloat(30.0)
        backDownButton.frame = CGRect(x: x, y: y, width: width, height: width)
        backDownButton.layer.shadowColor = UIColor.lightGray.cgColor
        backDownButton.layer.cornerRadius = width / 2
        backDownButton.layer.masksToBounds = false
        backDownButton.layer.shadowOffset = CGSize(width: 0, height: 2)
        backDownButton.layer.shadowOpacity = 0.3
    }
}

let images = [#imageLiteral(resourceName: "account.png"),#imageLiteral(resourceName: "calendar.png"),#imageLiteral(resourceName: "information.png"),#imageLiteral(resourceName: "question.png"), #imageLiteral(resourceName: "logout")]
let titles = ["NgÆ°á»i dĂ¹ng", "Lá»‹ch Ä‘Ă£ Ä‘áº·t", "ThĂ´ng tin á»©ng dá»¥ng", "Trá»£ giĂºp", "ThoĂ¡t"]


extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 140
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BigCell") as! BigCell
            cell.avatarImageView.image = images[0]
            cell.userNameLabel.text = titles[0]
            cell.avatarImageView.layer.cornerRadius = 30.0
            cell.avatarImageView.clipsToBounds = true
            cell.avatarImageView.layer.borderWidth = 1.0
            cell.avatarImageView.layer.borderColor = UIColor.gray.cgColor
            cell.avatarImageView.center.x = cell.center.x
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! NormalCell
        cell.cellImageView.image = images[indexPath.row]
        cell.cellTitle.text = titles[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 4 {
            let loginVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
            guard let window = UIApplication.shared.keyWindow else { return }
            UIView.transition(with: window, duration: 0.5, options: .transitionCurlDown, animations: {
                window.rootViewController = loginVc
                window.makeKeyAndVisible()
            })
        }
    }
}

class NormalCell: UITableViewCell {
    
    let cellImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    let cellTitle: UILabel = {
        let lb = UILabel()
        lb.contentMode = .center
        lb.textAlignment = .justified
        return lb
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(cellTitle)
        self.addSubview(cellImageView)
        separatorInset.left = 0
        addContraintsWithFormat(format: "H:|-10-[v0(30)]-15-[v1]|", views: cellImageView, cellTitle)
        addContraintsWithFormat(format: "V:|-20-[v0(20)]", views: cellImageView)
        addContraintsWithFormat(format: "V:|-20-[v0(20)]", views: cellTitle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class BigCell: UITableViewCell {
    
    let avatarImageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    let userNameLabel: UILabel = {
        let lb = UILabel()
        lb.contentMode = .center
        lb.textAlignment = .center
        return lb
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        separatorInset.left = 0
        addSubview(avatarImageView)
        addSubview(userNameLabel)
        
        addContraintsWithFormat(format: "H:[v0(60)]", views: avatarImageView)
        let xConstraint = NSLayoutConstraint(item: avatarImageView,
                                             attribute: .centerX,
                                             relatedBy: .equal,
                                             toItem: self, attribute: .centerX,
                                             multiplier: 1,
                                             constant: 0)
        NSLayoutConstraint.activate([xConstraint])
        avatarImageView.center.x = self.center.x
        addContraintsWithFormat(format: "H:|[v0]|", views: userNameLabel)
        addContraintsWithFormat(format: "V:|-20-[v0(60)]-10-[v1(30)]", views: avatarImageView, userNameLabel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}







