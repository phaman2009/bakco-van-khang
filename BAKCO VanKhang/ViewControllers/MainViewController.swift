//
//  ViewController.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 10/19/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import UIKit
import CoreLocation
import DynamicColor
import Hero

class MainViewController: BaseViewController, CLLocationManagerDelegate  {
    
    @IBOutlet var greenButton: UIButton!  //Hospitals Button
    @IBAction func greenButtonAction(_ sender: Any) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "HospitalViewController")
//        navigationController?.pushViewController(vc!, animated: true)
        let next = self.storyboard?.instantiateViewController(withIdentifier: "ChooseInformViewController")
        navigationController?.pushViewController(next!, animated: true)
    }
    
    @IBOutlet var blueButton: UIButton! //Another Button
    @IBAction func blueButtonAction(_ sender: Any) {
        let popUpVc = storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
        tabBarController?.view.addSubview((popUpVc.view)!)
        tabBarController?.addChildViewController(popUpVc)
    }
    
    @IBOutlet var advisoryButton: UIButton!
    @IBAction func advisoryButtonAction(_ sender: Any) {
    }
    
    let sosButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icon-sos-ok"), for: .normal)
        button.setImage(#imageLiteral(resourceName: "icon-sos-ok").withRenderingMode(.alwaysTemplate), for: .selected)
        button.addTarget(self, action: #selector(touchDown), for: .touchDown)
        button.addTarget(self, action: #selector(touchRelease), for: .touchUpInside)
        return button
    }()
    
    let label: UILabel = {
       let lb = UILabel()
        let attributes = ([NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 13.0),
                          NSAttributedStringKey.foregroundColor: UIColor.red])
        lb.attributedText = NSAttributedString(string: "Nhấn giữ để thực hiện cuộc gọi khẩn cấp!",
                                               attributes: attributes as [NSAttributedStringKey: Any])
        lb.contentMode = .center
        lb.textAlignment = .center
        return lb
    }()
    
    var locationManager:CLLocationManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(sosButton)
        view.addSubview(label)
        view.backgroundColor = .white
        
        blueButton.layer.cornerRadius = 10.0
        blueButton.clipsToBounds = true
        blueButton.contentMode = .scaleToFill

        greenButton.layer.cornerRadius = 10.0
        greenButton.clipsToBounds = true

        setupSOSButton()
        setupUserRightBarButton()
    }
    
    func setupSOSButton() {
        let buttonWidth = view.bounds.size.height / 4
        sosButton.frame = CGRect(x: (view.frame.size.width - buttonWidth) / 2,
                                 y: 100,
                                 width: buttonWidth,
                                 height: buttonWidth)
        sosButton.center.x = view.center.x
        sosButton.center.y = view.center.y - 150
        sosButton.layer.cornerRadius = buttonWidth / 2
        sosButton.clipsToBounds = true
        let anotherWidth = sosButton.frame.size.width / 10
        sosButton.contentEdgeInsets = UIEdgeInsets(top: -anotherWidth,
                                                   left: -anotherWidth,
                                                   bottom: -anotherWidth - 6,
                                                   right: -anotherWidth - 8)
        sosButton.layer.borderColor = UIColor.black.cgColor
        sosButton.layer.borderWidth = 1
        ///Set label at bottom of the SOS button.
        label.frame = CGRect(x: 0,
                             y: sosButton.center.y + (buttonWidth / 2) + 10 ,
                             width: view.frame.width,
                             height: 30)
    }
    
    
    
    @objc func touchDown() {
        print("You touched down the sos Button")
    }
    
    @objc func touchRelease() {
        print("Touched release")
    }
    
    func makeACall(scheme: String) {
        if let url = URL(string: scheme) {
            if(UIApplication.shared.canOpenURL(url))
            {
                if #available(iOS 10, *)
                {
                    UIApplication.shared.open(url, options: [:],
                                              completionHandler: {
                                                (success) in
                                                print("Open \(scheme): \(success)")})
                }
                else
                {
                    let success = UIApplication.shared.openURL(url)
                    print("Open \(scheme): \(success) 9")
                }
            }
            else
            {
                print ("error")
            }
        }
    }
    
    ///location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
    
    func checkCoreLocationPermission(){
       
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse{
            locationManager.startUpdatingLocation()
        }
        else if CLLocationManager.authorizationStatus() == .notDetermined{
            locationManager.requestWhenInUseAuthorization()
        }
        else if CLLocationManager.authorizationStatus() == .restricted{
            print("unauthorized")
        }
    }
}














