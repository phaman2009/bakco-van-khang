//
//  LoginViewController.swift
//  BAKCO VanKhang
//
//  Created by Kiet on 12/6/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import UIKit
import DynamicColor

class LoginViewController: UIViewController {
    
    @IBOutlet var buttonView: UIView!
    @IBOutlet var loginLaterButton: UIButton!
    @IBAction func loginLaterButtonAction(_ sender: Any) {
        let mainTab = self.storyboard?.instantiateViewController(withIdentifier: "tab")
        guard let window = UIApplication.shared.keyWindow else { return }
        UIView.transition(with: window, duration: 0.5, options: .transitionCrossDissolve, animations: {
            window.rootViewController = mainTab
            window.makeKeyAndVisible()
        }, completion: nil)
    }
    
    @IBOutlet var loginButton: UIButton!
    @IBAction func loginButtonAction(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "SecondMainViewController")
        let nav = BaseNavigationController(rootViewController: next!)
        nav.isHeroEnabled = false
        self.present(nav, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    func setupUI() {
        buttonView.layer.cornerRadius = buttonView.bounds.height / 2
        buttonView.clipsToBounds = true
        loginLaterButton.layer.cornerRadius = 8.0
        loginLaterButton.clipsToBounds = true
        loginLaterButton.layer.borderColor = DynamicColor(hexString: "74C7B7").cgColor
        loginLaterButton.layer.borderWidth = 0.5
        loginButton.layer.cornerRadius = 9.0
        loginButton.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    


}










