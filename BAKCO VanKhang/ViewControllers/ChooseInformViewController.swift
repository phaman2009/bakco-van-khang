//
//  ChooseInformViewController.swift
//  BAKCO VanKhang
//
//  Created by Kiet on 12/6/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import UIKit
import DynamicColor
import Alamofire
import MBProgressHUD
import IQDropDownTextField

class ChooseInformViewController: BaseViewController {
    
    //Property:
    
    var selectedHospital: HospitalModel?
    var selectedSpecialty: SpecialtyModel?
    var selectedUser: CustomerModel?
    var insurance = false
    
    let exList = ["Khám thông thường", "Khám dịch vụ", "Khám chuyên gia"]

    @IBOutlet var dateCollectionView: UICollectionView!
    @IBOutlet var timeInDayCollectionView: UICollectionView!
    @IBOutlet var confirmButton: UIButton!
    @IBOutlet var specialtyTextField: UITextField!
    @IBOutlet var exTypeDropDownTextField: IQDropDownTextField!
    @IBOutlet var hospitalNameTextField: UITextField!
    @IBOutlet var yesButton: UIButton!
    @IBOutlet var noButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showBackButton()
        setupDropDownTextField()
    }
    
    func setupUserInterface() {
        self.title = "Đăng kí khám bệnh"
        self.confirmButton.layer.cornerRadius = 10.0
        self.confirmButton.clipsToBounds = true
    }
    
    func setupDropDownTextField() {
        self.exTypeDropDownTextField.isOptionalDropDown = false
        self.exTypeDropDownTextField.itemList = exList
        self.exTypeDropDownTextField.showDismissToolbar = true
        self.yesButton.backgroundColor = UIColor.specialGreenColor()
        self.yesButton.setImage(UIImage(named: "no-image"), for: .normal)
        self.noButton.backgroundColor = UIColor.specialGreenColor()
        self.noButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
    }
    
    @IBAction func confirm(_ sender: Any) {
        
    }
    
    
    @IBAction func choosePaintient(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "InformationViewController")
        let nav = BaseNavigationController(rootViewController: next!)
        self.present(nav, animated: true)
    }
    
    @IBAction func chooseHospital(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "HospitalViewController") as! HospitalViewController
        next.delegate = self
        let nav = BaseNavigationController(rootViewController: next)
        self.present(nav, animated: true)
    }
    
    @IBAction func showSpecialtyList(_ sender: Any) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "ChooseSpecialtyViewController")
        let nav = BaseNavigationController(rootViewController: next!)
        self.present(nav, animated: true)
    }

    @IBAction func yes(_ sender: Any) {
        insurance = true
        yesButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        noButton.setImage(UIImage(named: "no-image"), for: .normal)
    }
    
    @IBAction func no(_ sender: Any) {
        insurance = false
        noButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
        yesButton.setImage(UIImage(named: "no-image"), for: .normal)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}



extension ChooseInformViewController: HopitalViewControllerDelegate, ChooseSpecialtyViewControllerDelegate {
    
    func didChooseSpecialty(specialty: SpecialtyModel) {
        specialtyTextField.text = specialty.Name
    }
    
    func didTapOnCell(hospital: HospitalModel) {
        hospitalNameTextField.text = hospital.Name
    }
    

}






// Lấy Người dùng ảo để test.
//    func getUserInfo() {
//        MBProgressHUD.showAdded(to: view, animated: true)
//        Alamofire.request(URL(string: "http://api.vkhs.vn/api/BkCustomer/GetById/15")!, method: .get).responseSwiftyJSON { (response) in
//            MBProgressHUD.hide(for: self.view, animated: true)
//            print(response.value as Any)
//            self.currentUser.initWithData(data: response.value?.dictionaryObject!)
//            self.userNames.append(self.currentUser.FullName!)
//
//        }
//    }









