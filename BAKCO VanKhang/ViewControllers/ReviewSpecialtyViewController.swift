//
//  ReviewSpecialtyViewController.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 11/14/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import MBProgressHUD

class ReviewSpecialtyViewController: BaseViewController, UIScrollViewDelegate {

    @IBOutlet var srollView: UIScrollView!
    @IBOutlet weak var lblHospitalName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblSpecialty: UILabel!
    @IBOutlet weak var lblPrice: UILabel!

    var hospitalName: String!
    var hospitalAddress: String!
    var hospitalService: String!
    var specialty: String!
    var price: Int!
    var isMember = true

    @IBOutlet weak var swtIsPatient: UISwitch!

    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtHealthInsurance: UITextField!

    var currentUser = CustomerModel()
    var currentMatch = MatchModel()
    var room = RoomModel()
    var hospital = HospitalModel()
    var service = ServiceModel()
    var detail = DetailModel()
    var customer = CustomerModel()
    var doctor = DoctorModel()


    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Xác nhận"
        getUserInfo()
        showBackButton()
        setupContinueButton()
        
        srollView.contentSize.width = srollView.bounds.width

        lblHospitalName.text = hospitalName
        lblAddress.text = "Địa chỉ: \(hospitalAddress!)"
        lblService.text = "Loại: \(hospitalService!)"
        lblSpecialty.text = "Khoa: \(specialty!)"
        lblPrice.text = "Chi phí: \(price!)đ"
    }
    
    func setupContinueButton() {
        let attribute = [NSAttributedStringKey.foregroundColor: UIColor.black]
        let nextButton = UIBarButtonItem(title: "Tiếp", style: .plain, target: self, action: #selector(moveNext))
        nextButton.setTitleTextAttributes(attribute, for: .normal)
        navigationItem.rightBarButtonItem = nextButton
    }

    @objc func moveNext() {
        getMatch()
    }

    @IBAction func actIsPatient(_ sender: Any) {
        if swtIsPatient.isOn {
            fillTextBox()
        }
        else {
            clearTextBox()
        }
    }

    func getUserInfo() {
        MBProgressHUD.showAdded(to: view, animated: true)
        Alamofire.request(URL(string: "http://api.vkhs.vn/api/BkCustomer/GetById/15")!, method: .get).responseSwiftyJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            print(response.value as Any)
            DispatchQueue.main.async {
                self.currentUser.initWithData(data: response.value?.dictionaryObject!)
                self.fillTextBox()
            }
        }
    }

    func getMatch() {
        var urlApiGetMatch: String!
        var parameters: Parameters

        if(isMember == true) {
            urlApiGetMatch = "http://api.vkhs.vn/api/BkMedicalExaminationNote/CreateForMember"
            parameters = ["CustomerId": 15, "HospitalId": 1, "HealthCareId": 4, "IsMorning": true, "Date": "2017-12-15", "Type": 0]
        } else {
            urlApiGetMatch = "http://api.vkhs.vn/api/BkMedicalExaminationNote/CreateForNonmember"
            let member = [
                "FullName": txtFullName.text!,
                "Phone": txtPhone.text!,
                "Email": txtEmail.text!,
                "HealthInsurance": txtHealthInsurance.text!,
                "Password": "123"
            ]
            parameters = ["HospitalId": 1, "HealthCareId": 4, "IsMorning": true, "Member": member]
        }
        MBProgressHUD.showAdded(to: view, animated: true)
        Alamofire.request(URL(string: urlApiGetMatch)!, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseSwiftyJSON { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            print(response.value as Any)
            DispatchQueue.main.async {
                self.currentMatch.initWithData(data: response.value?.dictionaryObject!)
                self.getAppointment(_healthCareSchedulerId: self.currentMatch.HealthCareSchedulerId!, _customerId: self.currentMatch.CustomerId!)
            }
        }
    }

    func getAppointment(_healthCareSchedulerId: Int, _customerId: Int) {
        
        let urlApiGetAppointment = "http://api.vkhs.vn/api/BkMedicalExaminationNote/Create"
        
        let parameters: Parameters = ["HealthCareSchedulerId": _healthCareSchedulerId,
                                      "CustomerId": _customerId,
                                      "PatientId": _customerId,
                                      "Type": 0]
        
        Alamofire.request(URL(string: urlApiGetAppointment)!,
                          method: .post,
                          parameters: parameters,
                          encoding: JSONEncoding.default).responseSwiftyJSON { (response) in
                            
                            //Parse to models
            print("/n/nResponse ở đay nhé! \n\(response.value as Any)")
            response.value?.forEach({ (string, json) in
                switch string {
                case "Room":
                    self.room.initWithData(data: json.dictionaryObject)
                    //print("\nRoom:\(room.Id)")
                    break
                case "Hospital":
                    self.hospital.initWithData(data: json.dictionaryObject!)
                    //   print("\nHospital address:\(hospital.Address!)")
                    break
                case "Detail":
                    self.detail.initWithData(data: json.dictionaryObject!)
                    //print("\nTime:\(detail.Time!)")
                    break
                case "Customer":
                    self.customer.initWithData(data: json.dictionaryObject!)
                    //print("\nCustomer fullname:\(customer.FullName!)")
                    break
                case "Doctor":
                    self.doctor.initWithData(data: json.dictionaryObject!)
                    //print("\nDoctor fullname:\(doctor.FullName!)")
                    break
                default: break

                }

            })
            DispatchQueue.main.async {
                let reviewAppointmentScreen = self.storyboard?.instantiateViewController(withIdentifier: "ReviewAppointmentViewController") as! ReviewAppointmentViewController
                reviewAppointmentScreen.hospitalName = self.hospitalName
                reviewAppointmentScreen.hospitalAddress = self.hospitalAddress
                reviewAppointmentScreen.specialty = self.specialty
                reviewAppointmentScreen.patientName = "\(String(describing: self.customer.FullName!))"
                reviewAppointmentScreen.date = "Ngày: \(String(describing: self.detail.Time!))"
                reviewAppointmentScreen.time = "Giờ: \(String(describing: self.detail.From!))"
                reviewAppointmentScreen.roomNumber = "Số phòng: \(String(describing: self.room.Name!))"
                self.navigationController?.pushViewController(reviewAppointmentScreen, animated: true)
            }
        }
    }

    func clearTextBox() {
        txtFullName.text = ""
        txtPhone.text = ""
        txtEmail.text = ""
        txtHealthInsurance.text = ""
        isMember = false
    }

    func fillTextBox() {
        txtFullName.text = self.currentUser.FullName
        txtPhone.text = self.currentUser.Phone
        txtEmail.text = self.currentUser.Email
        txtHealthInsurance.text = self.currentUser.HealthInsurance
        isMember = true
    }

    func textFieldDidChange(textField: UITextField) {
        if txtFullName.text == "" || txtPhone.text == "" || txtEmail.text == "" || txtHealthInsurance.text == "" {
            //Disable button
            print("dis")
        } else {
            print("ena")
        }
    }
}











//            response.value?.array?.forEach({ (json) in
//                var model = AppointmentModel()
//                model.parse(data: json.dictionaryObject)
//                print("\nhospital name\(String(describing: model.Hospital?.Name!))")
//
//            })
























