//
//  InformationViewController.swift
//  BAKCO VanKhang
//
//  Created by Kiet on 12/13/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import UIKit

class InformationViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Thông tin đăng kí"
        showCancelButton()
    }
    
    override func popToBack() {
        self.dismiss(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    


}
