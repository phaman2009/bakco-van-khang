//
//  HospitalCollectionCell.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 11/14/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import UIKit

class HospitalCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var hospitalLogo: UIImageView!
    @IBOutlet var hospitalNameLabel: UILabel!
    
}
