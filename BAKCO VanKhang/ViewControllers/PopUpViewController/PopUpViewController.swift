//
//  PopUpViewController.swift
//  BAKCO VanKhang
//
//  Created by Kiet on 11/24/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController {
    
    @IBOutlet private weak var viewPopupUI:UIView!
    @IBOutlet private weak var viewMain:UIView!
    @IBOutlet var dismissPopUpButton: UIButton!
    @IBOutlet var popUpCollectionView: UICollectionView!
    
    let images = [#imageLiteral(resourceName: "MedicalDoctor"), #imageLiteral(resourceName: "nurse"), #imageLiteral(resourceName: "chamSoc"), #imageLiteral(resourceName: "vatlitrilieu"), #imageLiteral(resourceName: "yHocCoTruyen3")]
    let cellNames = ["Bác sĩ gia đình", "Điều dưỡng", "Chăm sóc giảm nhẹ", "Vật lí trị liệu", "Y học cô truyền"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showViewWithAnimation()
        viewPopupUI.layer.cornerRadius = 15.0
        viewPopupUI.clipsToBounds = true
        popUpCollectionView.delegate = self
        popUpCollectionView.dataSource = self
        popUpCollectionView.register(MenuCell.self, forCellWithReuseIdentifier: "cellId")
        popUpCollectionView.contentInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func dismissPopUp(_ sender: Any) {
        self.hideViewWithAnimation()
    }
    
    //MARK: - Animation Method
    
    private func showViewWithAnimation() {
        
        self.view.alpha = 0
        self.viewPopupUI.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        
        UIView.animate(withDuration: 0.3) {
            self.viewPopupUI.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.view.alpha = 1
        }
    }
    
    private func hideViewWithAnimation() {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.viewPopupUI.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.view.alpha = 0
            
        }, completion: {
            (value: Bool) in
            
            self.removeFromParentViewController()
            self.view.removeFromSuperview()
        })
    }
}

extension PopUpViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! MenuCell
        cell.imageView.image = images[indexPath.item]
        cell.imageView.layer.cornerRadius = 15.0
        cell.imageView.clipsToBounds = true
        cell.imageView.layer.borderWidth = 1.0
        cell.imageView.layer.borderColor = UIColor.black.cgColor
        
        let fontAttribute = [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 11.0)] as [NSAttributedStringKey : Any]
        cell.nameLabel.attributedText = NSAttributedString(string: cellNames[indexPath.item],
                                                                   attributes: fontAttribute)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 70, height: 82)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

class MenuCell: UICollectionViewCell {
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleToFill
        return iv
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = NSTextAlignment.center
        label.numberOfLines = 0
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        tintColor = .red
        addSubview(imageView)
        addSubview(nameLabel)
        addContraintsWithFormat(format: "H:|-10-[v0(50)]-10-|", views: imageView)
        addContraintsWithFormat(format: "V:|[v0(50)]-2-[v1(30)]|", views: imageView, nameLabel)
        addContraintsWithFormat(format: "H:|[v0]|", views: nameLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
