//
//  BkHealthCareSchedulerPresenter.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 11/7/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation

protocol BkHealthCareSchedulerPresenterProtocol
{
    func refreshHealthCareScheduler(array: [HealthCareSchedulerModel])
}

class BkHealthCareSchedulerPresenter:NSObject{
    var presenter: BkHealthCareSchedulerPresenterProtocol!    
    init(presenter:BkHealthCareSchedulerPresenterProtocol) {
        self.presenter = presenter;
    }
    func getBkHealthCareScheduler(hospitalId:Int, healthcareId:Int) {
        let getHealthCareSchedulerApiUrl = URL(string: "http://120.72.85.132:8099/api/BkHealthCareScheduler/GetByHospitalHealthCareId?HospitalId=\(hospitalId)&HealthCareId=\(healthcareId)")
        let getHealthCareSchedulerRequest = URLRequest(url: getHealthCareSchedulerApiUrl!)
        URLSession.shared.dataTask(with: getHealthCareSchedulerRequest) { (healthcareSchedulerData, response, error) in
            do{                
                let healthcareSchedulerJson = try JSONSerialization.jsonObject(with: healthcareSchedulerData!, options: .allowFragments) as! [AnyObject]
                var healthcareSchedulerList:[HealthCareSchedulerModel] = []
                for hcSO in healthcareSchedulerJson{
                    let morning = hcSO["Morning"] as! [AnyObject]
                    let afternoon = hcSO["Afternoon"] as! [AnyObject]
                    var morningList:[Spell] = []
                    var afternoonList:[Spell] = []
                    for m in morning{
                        morningList.append(
                            Spell(
                                timeId:m["TimeId"] as! Int,
                                time:m["Time"] as! String,
                                from:m["From"] as! String,
                                to:m["To"] as! String,
                                active:m["Active"] as! Bool
                            )
                        )
                    }
                    for a in afternoon{
                        afternoonList.append(
                            Spell(
                                timeId:a["TimeId"] as! Int,
                                time:a["Time"] as! String,
                                from:a["From"] as! String,
                                to:a["To"] as! String,
                                active:a["Active"] as! Bool
                            )
                        )
                    }
                    healthcareSchedulerList.append(
                        HealthCareSchedulerModel(
                            date: hcSO["Date"] as! String,
                            dateView: hcSO["DateView"] as! String,
                            morning: morningList,
                            afternoon: afternoonList
                        )
                    )
                }
                DispatchQueue.main.async {
                    self.presenter.refreshHealthCareScheduler(array: healthcareSchedulerList)
                }
            }catch{}
            }.resume()
    }
}
