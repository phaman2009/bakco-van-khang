//
//  Presenter.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 10/20/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation
import CoreLocation


class MainPresenter{
    
    weak fileprivate var mainView : MainViewController?
    
    func attachView(_ view:MainViewController){
        mainView = view
        
    }
    
    func detachView() {
        mainView = nil
    }
    
    
}
