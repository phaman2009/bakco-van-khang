//
//  BkHealthCarePresenter.swift
//  BAKCO VanKhang
//
//  Created by Pham An on 10/23/17.
//  Copyright © 2017 Pham An. All rights reserved.
//

import Foundation

protocol BkHealthCarePresenterProtocol
{
    func refreshHealthCare(array: [HealthCareModel])
    func refreshHospitals(array: [HospitalModel])
}

class BkHealthCarePresenter: NSObject {
    var presenter: BkHealthCarePresenterProtocol!
    
    init(presenter:BkHealthCarePresenterProtocol) {
        self.presenter = presenter;
    }
    
    func getHealthCare() {
        let getHealthCareApiUrl = URL(string: "http://120.72.85.132:8099/api/BkHealthCare/Get")
        let getHealthCareRequest = URLRequest(url: getHealthCareApiUrl!)
        var healthcareList:[HealthCareModel] = []
        URLSession.shared.dataTask(with: getHealthCareRequest) { (healthcareData, response, error) in
            do{
                let healthcareJson = try JSONSerialization.jsonObject(with: healthcareData!, options: .allowFragments) as! [AnyObject]
                for healthcare in healthcareJson{
                    healthcareList.append(
                        HealthCareModel(
                            Id   : healthcare["Id"] as! Int,
                            Name : healthcare["Name"] as! String,
                            Price: healthcare["Price"] as! Int,
                            Image: healthcare["Image"] as! String
                        )
                    )
                }
                DispatchQueue.main.async {
                    self.presenter.refreshHealthCare(array: healthcareList)
                }
            }catch{}
            }.resume()
    }
    func getHospitalByHealthCare(HealthCareId: Int) {
        let getHospitalApiUrl = URL(string: "http://120.72.85.132:8099/api/BkHospital/GetByHealthCareId/\(HealthCareId)")
        let getHospitalRequest = URLRequest(url: getHospitalApiUrl!)
        var hospitalList:[HospitalModel] = []
        URLSession.shared.dataTask(with: getHospitalRequest) { (hospitalData, response, error) in
            do{
                let hospitalJson = try JSONSerialization.jsonObject(with: hospitalData!, options: .allowFragments) as! [AnyObject]
                for hospital in hospitalJson{
                    hospitalList.append(
                        HospitalModel(
                            Id   : hospital["Id"] as! Int,
                            Name: hospital["Name"] as? String,
                            Image: hospital["Image"] as? String,
                            Website: hospital["Website"] as? String,
                            Price: hospital["Price"] as? Int,
                            Address: hospital["Address"] as? String
                        )
                    )
                }
                DispatchQueue.main.async {
                    self.presenter.refreshHospitals(array: hospitalList)
                }
            }catch{}
            }.resume()        
    }
}
